#include <stdio.h>
#include <math.h>
#include <minpack.h>
#define real __minpack_real__

void fcn(const int*,const int*,const real*,real*,int*);

real computarFunction(real y,real x0,real x1,real x2,real tmp1,real tmp2,real tmp3){
    return y - (x0 + tmp1*x1/tmp2 - tmp3*x2);
}

void fcn(const int* m,const int* n, const real *x, real* fvec, int *iflag){
    real y[15]={1.4e-1,1.8e-1,2.2e-1,2.5e-1,2.9e-1,3.2e-1,3.5e-1,3.9e-1,
          3.7e-1,5.8e-1,7.3e-1,9.6e-1,1.34e0,2.1e0,4.39e0};
    for (int i=0; i< *m; i++){
        real tmp1 = i+1;
        real tmp2 = 15 - i;
        real tmp3 = tmp1 < tmp2 ? tmp1 : tmp2;
//        fvec[i] = y[i] -(x[0] + tmp1*x[1]/tmp2 - tmp3*x[2]);
        fvec[i] = computarFunction(y[i],x[0],x[1],x[2],tmp1,tmp2,tmp3);
      }
}

int main(){
    int m = 15, n = 3, lwa, iwa[3], one = 1, info;
    real ftol, fnorm, x[3], fvec[15], wa[75];

    x[0] = 1.e0; x[1] = 1.e0; x[2] = 1.e0;
    lwa = 75;

    ftol = sqrt(__minpack_func__(dpmpar)(&one));
    __minpack_func__(lmdif1)(&fcn, &m, &n, x, fvec, &ftol, &info, iwa, wa, &lwa);
    fnorm = __minpack_func__(enorm)(&m, fvec);

    printf("      FINAL L2 NORM OF THE RESIDUALS%15.7f\n\n",fnorm);
    printf("      EXIT PARAMETER                %10i\n\n", info);
    printf("      FINAL APPROXIMATE SOLUTION\n\n %15.7f%15.7f%15.7f\n",
       x[0], x[1], x[2]);
    return 0;
}