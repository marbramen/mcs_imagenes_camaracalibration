#include "KFMultiTracking.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>

#include <vector>
#include <iostream>

using namespace std;
using namespace cv;

#define MIN_H_BLUE 200
#define MAX_H_BLUE 300
#define MAX_FRAMES_NOT_FOUND 3

int main(){
    string nameVideo = "pelotas_01_003_30.avi";
    string path = "/home/cesar/CesarBragagnini/MCS/Imagenes/benchMarking/videos/";
    cv::VideoCapture cap(path + nameVideo);
    if(!cap.isOpened()){
        cout << "no abrio el video " << endl;
        return false;
    }
    char ch = 0;
    double ticks = 0;
    bool firstFound = false;
    int frFouBall = 0;
    Mat frame, res;

    KFMultiTracking* kfMT;

    while (ch != 'q' || ch != 'Q'){
        double precTick = ticks;
        ticks = (double)getTickCount();
        double dT = (ticks - precTick) / cv::getTickFrequency();

        cap >> frame;
        frame.copyTo(res);

        // PHASE DETECTION
        Mat blur;
        GaussianBlur(frame, blur, cv::Size(5, 5), 3.0, 3.0);
        Mat frmHsv;
        cvtColor(blur, frmHsv, CV_BGR2HSV);
        Mat rangeRes = Mat::zeros(frame.size(), CV_8UC1);
        inRange(frmHsv, Scalar(MIN_H_BLUE / 2, 100, 80), cv::Scalar(MAX_H_BLUE / 2, 255, 255), rangeRes);
        erode(rangeRes, rangeRes, Mat(), cv::Point(-1, -1), 2);
        dilate(rangeRes, rangeRes, Mat(), cv::Point(-1, -1), 2);
        vector<vector<Point> > contours;
        findContours(rangeRes, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
        vector<vector<Point> > balls;
        vector<Rect> ballsBox;
        vector<Rect> ballsBoxPrev;
        for (size_t i = 0; i < contours.size(); i++){
            Rect bBox;
            bBox = boundingRect(contours[i]);
            float ratio = (float) bBox.width / (float) bBox.height;
            if (ratio > 1.0f)
                ratio = 1.0f / ratio;
            if (ratio > 0.50 && bBox.area() >= 400){
                balls.push_back(contours[i]);
                ballsBox.push_back(bBox);
            }
        }
        cout << "Balls found:" << ballsBox.size() << endl;
        for (size_t i = 0; i < balls.size(); i++){
            drawContours(res, balls, i, CV_RGB(20,150,20), 1);
            rectangle(res, ballsBox[i], CV_RGB(0,255,0), 2);

            Point center;
            center.x = ballsBox[i].x + ballsBox[i].width / 2;
            center.y = ballsBox[i].y + ballsBox[i].height / 2;
            circle(res, center, 5, CV_RGB(20,150,20), -1);

            stringstream sstr;
            sstr << "(" << center.x << "," << center.y << ")";
            putText(res, sstr.str(), cv::Point(center.x + 3, center.y - 3), cv::FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(20,150,20), 2);
        }

        // PHASE KALMAN
        if(!ballsBox.empty()){
            vector<Mat>* arrayMat = new vector<Mat>();
            for(int i = 0; i < (int)ballsBox.size(); i++){
                Mat temp(MEASURE_SIZE,1, TYPE_KF);
                temp.at<float>(0) = ballsBox[i].x + ballsBox[i].width / 2;
                temp.at<float>(1) = ballsBox[i].y + ballsBox[i].height / 2;
                temp.at<float>(2) = (float)ballsBox[i].width;
                temp.at<float>(3) = (float)ballsBox[i].height;
                (*arrayMat).push_back(temp);
            }
            if(firstFound){
                firstFound = false;
                kfMT = new KFMultiTracking(1);
                kfMT->setStateInit(arrayMat);

                firstFound = false;
            }else{
                kfMT->predict(dT);
                vector<Mat>* correct = kfMT->kalmanCorrection(arrayMat);
                vector<Mat>* matForPre = kfMT->futureNTime(2);

                for(int i = 0; i < (int)correct->size(); i++){
//                    int width = (*correct)[i].at<float>(4);
//                    int height =  (*correct)[i].at<float>(5);
//                    int px = (*correct)[i].at<float>(0) - width / 2 ;
//                    int py = (*correct)[i].at<float>(1) - height / 2;
//                    Rect predRect(px, py, width, height);
//                    Point predPoint((*correct)[i].at<float>(0), (*correct)[i].at<float>(1));
//                    rectangle(res, predRect, CV_RGB(255,0,0), 2);
//                    circle(res, predPoint, 5, CV_RGB(255,0,0),-1);

//                    int widthPred = (*matForPre)[i].at<float>(4);
//                    int heightPred =  (*matForPre)[i].at<float>(5);
//                    int pxPred = (*matForPre)[i].at<float>(0) - widthPred / 2 ;
//                    int pyPred = (*matForPre)[i].at<float>(1) - heightPred / 2;
//                    Rect predRect2(pxPred, pyPred, widthPred, heightPred);
//                    Point predPoint2((*matForPre)[i].at<float>(0), (*matForPre)[i].at<float>(1));
//                    rectangle(res, predRect2, CV_RGB(255,255,0), 2);
                    Point iniArrow((*correct)[i].at<float>(0), (*correct)[i].at<float>(1));
                    Point finArrow((*matForPre)[i].at<float>(0), (*matForPre)[i].at<float>(1));
                    circle(res, finArrow, 3, CV_RGB(255,255,0),-1);
                    line(res,iniArrow,finArrow,CV_RGB(255,255,0),1,8,0);

                }
            }
        }else{
            frFouBall++;
            if(frFouBall > MAX_FRAMES_NOT_FOUND){
                firstFound = true;
            }
        }
        ballsBoxPrev = ballsBox;
        cv::imshow("Resutaldo final", res);
        ch = cv::waitKey(500);
    }
    return 0;
}

