#-------------------------------------------------
#
# Project created by QtCreator 2016-12-18T19:10:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TrackingBall
TEMPLATE = app


SOURCES +=\
        mainwindow.cpp \
    mainTracBall.cpp \
    KFMultiTracking.cpp \
    mainTracBall2.cpp

HEADERS  += mainwindow.h \
    KFMultiTracking.h

FORMS    += mainwindow.ui

#To check included Path :  	$ pkg-config --cflags opencv
#To check library :  		$ pkg-config --libs opencv

INCLUDEPATH += /usr/local/include/opencv
LIBS += `pkg-config --cflags --libs opencv`

# -L represents for Directory
# -l represents for file
