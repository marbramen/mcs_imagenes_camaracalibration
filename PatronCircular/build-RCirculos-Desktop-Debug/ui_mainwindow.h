/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *winProc1;
    QLabel *winFinalRes;
    QLabel *winProc2;
    QLabel *winProc3;
    QLabel *winProc4;
    QPushButton *btnLoadVideo;
    QPushButton *btnInitProc;
    QLabel *lblMsg;
    QGroupBox *groupBox;
    QRadioButton *rbCircle;
    QRadioButton *rbRing;
    QLineEdit *numRows;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *numCols;
    QLabel *label_5;
    QLineEdit *centersDistance;
    QLabel *winProc5;
    QLabel *winProc6;
    QLabel *winProc7;
    QLabel *winProc8;
    QLabel *winProc9;
    QLabel *subtitle3;
    QLabel *subtitle2;
    QLabel *subtitle1;
    QLabel *subtitle4;
    QLabel *subtitle5;
    QLabel *subtitle6;
    QLabel *subtitle7;
    QLabel *subtitle8;
    QLabel *subtitle9;
    QLabel *subtitle3_2;
    QGroupBox *groupBox_2;
    QLabel *label_3;
    QLineEdit *calNumFrames;
    QLabel *label_4;
    QLineEdit *calOutputFile;
    QCheckBox *withCalibration;
    QCheckBox *fixAspectRatio;
    QCheckBox *fixPrincipalPoint;
    QCheckBox *zeroTangentDist;
    QGroupBox *groupBox_3;
    QLabel *label_6;
    QLineEdit *fileDistance;
    QCheckBox *withDistance;
    QPushButton *btnLoadFileDist;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1500, 900);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        winProc1 = new QLabel(centralWidget);
        winProc1->setObjectName(QString::fromUtf8("winProc1"));
        winProc1->setGeometry(QRect(20, 30, 261, 231));
        winProc1->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc1->setScaledContents(true);
        winFinalRes = new QLabel(centralWidget);
        winFinalRes->setObjectName(QString::fromUtf8("winFinalRes"));
        winFinalRes->setGeometry(QRect(870, 40, 611, 391));
        winFinalRes->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winFinalRes->setScaledContents(true);
        winProc2 = new QLabel(centralWidget);
        winProc2->setObjectName(QString::fromUtf8("winProc2"));
        winProc2->setGeometry(QRect(300, 30, 261, 231));
        winProc2->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc2->setScaledContents(true);
        winProc3 = new QLabel(centralWidget);
        winProc3->setObjectName(QString::fromUtf8("winProc3"));
        winProc3->setGeometry(QRect(580, 30, 261, 231));
        winProc3->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc3->setScaledContents(true);
        winProc4 = new QLabel(centralWidget);
        winProc4->setObjectName(QString::fromUtf8("winProc4"));
        winProc4->setGeometry(QRect(20, 310, 261, 231));
        winProc4->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc4->setScaledContents(true);
        btnLoadVideo = new QPushButton(centralWidget);
        btnLoadVideo->setObjectName(QString::fromUtf8("btnLoadVideo"));
        btnLoadVideo->setGeometry(QRect(1190, 750, 99, 27));
        btnInitProc = new QPushButton(centralWidget);
        btnInitProc->setObjectName(QString::fromUtf8("btnInitProc"));
        btnInitProc->setEnabled(false);
        btnInitProc->setGeometry(QRect(1320, 750, 99, 27));
        lblMsg = new QLabel(centralWidget);
        lblMsg->setObjectName(QString::fromUtf8("lblMsg"));
        lblMsg->setGeometry(QRect(1010, 790, 351, 31));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(890, 450, 291, 161));
        groupBox->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        rbCircle = new QRadioButton(groupBox);
        rbCircle->setObjectName(QString::fromUtf8("rbCircle"));
        rbCircle->setGeometry(QRect(10, 30, 131, 20));
        rbCircle->setChecked(true);
        rbRing = new QRadioButton(groupBox);
        rbRing->setObjectName(QString::fromUtf8("rbRing"));
        rbRing->setGeometry(QRect(160, 30, 121, 20));
        numRows = new QLineEdit(groupBox);
        numRows->setObjectName(QString::fromUtf8("numRows"));
        numRows->setGeometry(QRect(160, 100, 71, 21));
        numRows->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        numRows->setReadOnly(true);
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(24, 100, 111, 21));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(24, 70, 111, 21));
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        numCols = new QLineEdit(groupBox);
        numCols->setObjectName(QString::fromUtf8("numCols"));
        numCols->setGeometry(QRect(160, 70, 71, 21));
        numCols->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        numCols->setReadOnly(true);
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(24, 130, 111, 21));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        centersDistance = new QLineEdit(groupBox);
        centersDistance->setObjectName(QString::fromUtf8("centersDistance"));
        centersDistance->setGeometry(QRect(160, 130, 71, 21));
        centersDistance->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        centersDistance->setReadOnly(false);
        winProc5 = new QLabel(centralWidget);
        winProc5->setObjectName(QString::fromUtf8("winProc5"));
        winProc5->setGeometry(QRect(300, 310, 261, 231));
        winProc5->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc5->setScaledContents(true);
        winProc6 = new QLabel(centralWidget);
        winProc6->setObjectName(QString::fromUtf8("winProc6"));
        winProc6->setGeometry(QRect(580, 310, 261, 231));
        winProc6->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc6->setScaledContents(true);
        winProc7 = new QLabel(centralWidget);
        winProc7->setObjectName(QString::fromUtf8("winProc7"));
        winProc7->setGeometry(QRect(20, 590, 261, 231));
        winProc7->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc7->setScaledContents(true);
        winProc8 = new QLabel(centralWidget);
        winProc8->setObjectName(QString::fromUtf8("winProc8"));
        winProc8->setGeometry(QRect(300, 590, 261, 231));
        winProc8->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc8->setScaledContents(true);
        winProc9 = new QLabel(centralWidget);
        winProc9->setObjectName(QString::fromUtf8("winProc9"));
        winProc9->setGeometry(QRect(580, 590, 261, 231));
        winProc9->setStyleSheet(QString::fromUtf8("border: 1px solid gray;"));
        winProc9->setScaledContents(true);
        subtitle3 = new QLabel(centralWidget);
        subtitle3->setObjectName(QString::fromUtf8("subtitle3"));
        subtitle3->setGeometry(QRect(580, 10, 261, 17));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        subtitle3->setFont(font1);
        subtitle3->setAlignment(Qt::AlignCenter);
        subtitle2 = new QLabel(centralWidget);
        subtitle2->setObjectName(QString::fromUtf8("subtitle2"));
        subtitle2->setGeometry(QRect(300, 10, 261, 17));
        subtitle2->setFont(font1);
        subtitle2->setAlignment(Qt::AlignCenter);
        subtitle1 = new QLabel(centralWidget);
        subtitle1->setObjectName(QString::fromUtf8("subtitle1"));
        subtitle1->setGeometry(QRect(20, 10, 261, 17));
        subtitle1->setFont(font1);
        subtitle1->setAlignment(Qt::AlignCenter);
        subtitle4 = new QLabel(centralWidget);
        subtitle4->setObjectName(QString::fromUtf8("subtitle4"));
        subtitle4->setGeometry(QRect(20, 290, 261, 17));
        subtitle4->setFont(font1);
        subtitle4->setAlignment(Qt::AlignCenter);
        subtitle5 = new QLabel(centralWidget);
        subtitle5->setObjectName(QString::fromUtf8("subtitle5"));
        subtitle5->setGeometry(QRect(300, 290, 261, 17));
        subtitle5->setFont(font1);
        subtitle5->setAlignment(Qt::AlignCenter);
        subtitle6 = new QLabel(centralWidget);
        subtitle6->setObjectName(QString::fromUtf8("subtitle6"));
        subtitle6->setGeometry(QRect(580, 290, 261, 17));
        subtitle6->setFont(font1);
        subtitle6->setAlignment(Qt::AlignCenter);
        subtitle7 = new QLabel(centralWidget);
        subtitle7->setObjectName(QString::fromUtf8("subtitle7"));
        subtitle7->setGeometry(QRect(20, 570, 261, 17));
        subtitle7->setFont(font1);
        subtitle7->setAlignment(Qt::AlignCenter);
        subtitle8 = new QLabel(centralWidget);
        subtitle8->setObjectName(QString::fromUtf8("subtitle8"));
        subtitle8->setGeometry(QRect(300, 570, 261, 17));
        subtitle8->setFont(font1);
        subtitle8->setAlignment(Qt::AlignCenter);
        subtitle9 = new QLabel(centralWidget);
        subtitle9->setObjectName(QString::fromUtf8("subtitle9"));
        subtitle9->setGeometry(QRect(580, 570, 261, 17));
        subtitle9->setFont(font1);
        subtitle9->setAlignment(Qt::AlignCenter);
        subtitle3_2 = new QLabel(centralWidget);
        subtitle3_2->setObjectName(QString::fromUtf8("subtitle3_2"));
        subtitle3_2->setGeometry(QRect(1050, 10, 261, 17));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        subtitle3_2->setFont(font2);
        subtitle3_2->setAlignment(Qt::AlignCenter);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(1210, 450, 251, 211));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 150, 81, 21));
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        calNumFrames = new QLineEdit(groupBox_2);
        calNumFrames->setObjectName(QString::fromUtf8("calNumFrames"));
        calNumFrames->setEnabled(false);
        calNumFrames->setGeometry(QRect(100, 150, 51, 21));
        calNumFrames->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        calNumFrames->setReadOnly(false);
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 180, 81, 21));
        label_4->setFont(font);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        calOutputFile = new QLineEdit(groupBox_2);
        calOutputFile->setObjectName(QString::fromUtf8("calOutputFile"));
        calOutputFile->setEnabled(false);
        calOutputFile->setGeometry(QRect(100, 180, 141, 21));
        calOutputFile->setReadOnly(false);
        withCalibration = new QCheckBox(groupBox_2);
        withCalibration->setObjectName(QString::fromUtf8("withCalibration"));
        withCalibration->setGeometry(QRect(10, 30, 141, 22));
        fixAspectRatio = new QCheckBox(groupBox_2);
        fixAspectRatio->setObjectName(QString::fromUtf8("fixAspectRatio"));
        fixAspectRatio->setEnabled(false);
        fixAspectRatio->setGeometry(QRect(40, 60, 181, 22));
        fixAspectRatio->setChecked(false);
        fixPrincipalPoint = new QCheckBox(groupBox_2);
        fixPrincipalPoint->setObjectName(QString::fromUtf8("fixPrincipalPoint"));
        fixPrincipalPoint->setEnabled(false);
        fixPrincipalPoint->setGeometry(QRect(40, 85, 181, 22));
        fixPrincipalPoint->setChecked(false);
        zeroTangentDist = new QCheckBox(groupBox_2);
        zeroTangentDist->setObjectName(QString::fromUtf8("zeroTangentDist"));
        zeroTangentDist->setEnabled(false);
        zeroTangentDist->setGeometry(QRect(40, 110, 181, 22));
        zeroTangentDist->setChecked(true);
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(900, 630, 271, 131));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(20, 60, 67, 21));
        label_6->setFont(font);
        fileDistance = new QLineEdit(groupBox_3);
        fileDistance->setObjectName(QString::fromUtf8("fileDistance"));
        fileDistance->setEnabled(false);
        fileDistance->setGeometry(QRect(90, 60, 161, 21));
        fileDistance->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        fileDistance->setReadOnly(true);
        withDistance = new QCheckBox(groupBox_3);
        withDistance->setObjectName(QString::fromUtf8("withDistance"));
        withDistance->setGeometry(QRect(20, 30, 221, 21));
        btnLoadFileDist = new QPushButton(groupBox_3);
        btnLoadFileDist->setObjectName(QString::fromUtf8("btnLoadFileDist"));
        btnLoadFileDist->setEnabled(false);
        btnLoadFileDist->setGeometry(QRect(60, 90, 141, 27));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1500, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Camera Calibration", 0, QApplication::UnicodeUTF8));
        winProc1->setText(QString());
        winFinalRes->setText(QString());
        winProc2->setText(QString());
        winProc3->setText(QString());
        winProc4->setText(QString());
        btnLoadVideo->setText(QApplication::translate("MainWindow", "Load Video", 0, QApplication::UnicodeUTF8));
        btnInitProc->setText(QApplication::translate("MainWindow", "Start", 0, QApplication::UnicodeUTF8));
        lblMsg->setText(QString());
        groupBox->setTitle(QApplication::translate("MainWindow", "Patr\303\263n", 0, QApplication::UnicodeUTF8));
        rbCircle->setText(QApplication::translate("MainWindow", "Patr\303\263n circular", 0, QApplication::UnicodeUTF8));
        rbRing->setText(QApplication::translate("MainWindow", "Patr\303\263n anillo", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "N\302\260 elems/col:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "N\302\260 columnas:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Distancia (mm):", 0, QApplication::UnicodeUTF8));
        winProc5->setText(QString());
        winProc6->setText(QString());
        winProc7->setText(QString());
        winProc8->setText(QString());
        winProc9->setText(QString());
        subtitle3->setText(QString());
        subtitle2->setText(QString());
        subtitle1->setText(QString());
        subtitle4->setText(QString());
        subtitle5->setText(QString());
        subtitle6->setText(QString());
        subtitle7->setText(QString());
        subtitle8->setText(QString());
        subtitle9->setText(QString());
        subtitle3_2->setText(QApplication::translate("MainWindow", "RESULTADO FINAL", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Calibraci\303\263n", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "N\302\260 frames:", 0, QApplication::UnicodeUTF8));
        calNumFrames->setText(QApplication::translate("MainWindow", "25", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Salida:", 0, QApplication::UnicodeUTF8));
        calOutputFile->setText(QApplication::translate("MainWindow", "output.xml", 0, QApplication::UnicodeUTF8));
        withCalibration->setText(QApplication::translate("MainWindow", "Calibrar c\303\241mara", 0, QApplication::UnicodeUTF8));
        fixAspectRatio->setText(QApplication::translate("MainWindow", "Fix Aspect Ratio", 0, QApplication::UnicodeUTF8));
        fixPrincipalPoint->setText(QApplication::translate("MainWindow", "Fix Principal Point", 0, QApplication::UnicodeUTF8));
        zeroTangentDist->setText(QApplication::translate("MainWindow", "Zero Tangent Dist", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Distancia", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Archivo:", 0, QApplication::UnicodeUTF8));
        fileDistance->setText(QString());
        withDistance->setText(QApplication::translate("MainWindow", "Calcular distancia a c\303\241mara", 0, QApplication::UnicodeUTF8));
        btnLoadFileDist->setText(QApplication::translate("MainWindow", "Cargar archivo", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
