VS 2013 de 32bits
Qt 5.7

Despues de compilar el proyecto con Visual Studio, se crea una carpeta bin
- En nuestro caso se creo \bin\x32

Si se tiene problemas con algunas DLL:
- Descomprimir el archivo dlls_qt_vs.zip
- Copiar las DLLs en la siguiente ruta:
	\bin\x32\
- Volver a ejecutar el proyecto.
